package com.hendisantika.springbootelasticsearchdemo.exception;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elasticsearch-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/12/19
 * Time: 05.21
 */
public class BookNotFoundException extends Exception {

    public BookNotFoundException(String message) {
        super(message);
    }
}