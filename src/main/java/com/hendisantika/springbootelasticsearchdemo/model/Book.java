package com.hendisantika.springbootelasticsearchdemo.model;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elasticsearch-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/12/19
 * Time: 05.18
 */
@Document(indexName = "books", type = "book")
@Data
@Getter
@Setter
public class Book {
    @Id
    private String id;

    private String title;

    private int publicationYear;

    private String authorName;

    private String isbn;
}
