package com.hendisantika.springbootelasticsearchdemo.service;

import com.hendisantika.springbootelasticsearchdemo.exception.BookNotFoundException;
import com.hendisantika.springbootelasticsearchdemo.exception.DuplicateIsbnException;
import com.hendisantika.springbootelasticsearchdemo.model.Book;

import java.util.List;
import java.util.Optional;

/**
 * Created by IntelliJ IDEA.
 * Project : springboot-elasticsearch-demo
 * User: hendisantika
 * Email: hendisantika@gmail.com
 * Telegram : @hendisantika34
 * Date: 27/12/19
 * Time: 05.22
 */
public interface BookService {

    Optional<Book> getByIsbn(String isbn);

    List<Book> getAll();

    List<Book> findByAuthor(String authorName);

    List<Book> findByTitleAndAuthor(String title, String author);

    Book create(Book book) throws DuplicateIsbnException;

    void deleteById(String id);

    Book update(String id, Book book) throws BookNotFoundException;
}